<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TicketController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/tickets', [TicketController::class, 'index'])->name('tickets.index');
Route::get('tickets/create', [TicketController::class, 'create'])->name('tickets.create');
Route::post('tickets/store', [TicketController::class, 'store'])->name('tickets.store');
Route::get('tickets/{id}/show', [TicketController::class, 'show'])->name('tickets.show');


Route::group(['prefix' => 'admin', 'middleware' => ['is_admin']], function(){

  	Route::get('home', [HomeController::class, 'adminHome'])->name('admin.home');

	Route::get('/tickets', [TicketController::class, 'adminIndex'])->name('admin.tickets.index');
	Route::get('tickets/{id}/show', [TicketController::class, 'adminShow'])->name('admin.tickets.show');

	Route::patch('tickets/{id}/markClosed', [TicketController::class, 'markClosed'])->name('admin.tickets.markClosed');

});