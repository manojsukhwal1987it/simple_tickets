<?php

namespace App\Providers\App\Listeners;

use App\Providers\App\Event\TicketClosed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TicketClosedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\App\Event\TicketClosed  $event
     * @return void
     */
    public function handle(TicketClosed $event)
    {
        //
    }
}
