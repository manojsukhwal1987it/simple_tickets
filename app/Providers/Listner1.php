<?php

namespace App\Providers;

use App\Providers\Event1;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class Listner1
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\Event1  $event
     * @return void
     */
    public function handle(Event1 $event)
    {
        //
    }
}
