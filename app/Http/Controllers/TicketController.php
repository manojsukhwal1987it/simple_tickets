<?php

namespace App\Http\Controllers;
   
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Event;
use App\Events\TicketClosed;
  
class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $tickets = Ticket::where('user_id',Auth::user()->id);
        $sort = $request->query('sort','');
        $dir = $request->query('dir','');

        if(!empty($sort) && !empty($dir) && in_array($sort, ['title','status']) && in_array($dir, ['asc','desc'])) {
        	$tickets->orderBy($sort,$dir);
        } else {
        	$tickets->latest();	
        }

        $tickets = $tickets->paginate(10); // ->with(['ticketUser'])
    
        return view('tickets.index',compact('tickets'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('tickets.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $ticket = new Ticket;
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->user_id = Auth::user()->id;
        $ticket->save();

        return redirect()->route('tickets.index')->with('success','Ticket created successfully.');
            
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket, $tid)
    {
    	$ticket = Ticket::where('id', $tid)->where('user_id',Auth::user()->id)->firstOrFail();
        return view('tickets.view',compact('ticket'));
    } 
     
    
    public function adminIndex(Request $request)
    {

        $tickets = new Ticket;
        
        $title = $request->query('title','');
		if (!empty($title)) {
			$tickets = $tickets->where('title','like','%'.$title.'%');
		}        
		
        $tickets->with(['ticketUser']);
        $tickets->latest();

        $tickets = $tickets->paginate(10);

        return view('tickets.adminIndex',compact('tickets'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function adminShow(Ticket $ticket, $tid)
    {
    	$ticket = Ticket::where('id', $tid)->with(['ticketUser'])->firstOrFail();
        return view('tickets.adminView',compact('ticket'));
    }

    public function markClosed(Request $request, Ticket $ticket, $tid)
    {
    	
    	try {
	    	$ticket = Ticket::where('id', $tid)->firstOrFail();
	    	$ticket->status='closed';
	    	if ($ticket->save()) {

	    		//dd($ticket->id);

	    		//trigger the user notified event here
	    		Event::dispatch(new TicketClosed($ticket->id));

	    		return redirect()->route('admin.tickets.index')->with('success','Ticket has been closed successfully.');	
	    	}
    	} catch (\Exception $e) {
	    	//dd($e->getMessage());
	    	return redirect()->route('admin.tickets.index')->with('error','Something went wrong. please try again.');
    	}

    }

}
