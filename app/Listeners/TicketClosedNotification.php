<?php

namespace App\Listeners;

use App\Events\TicketClosed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\Ticket;
use Mail;
use Carbon\Carbon;


class TicketClosedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TicketClosed  $event
     * @return void
     */
    public function handle(TicketClosed $event)
    {
        try {

            //dd($event->ticket_id);
            //\DB::statement("UPDATE `tickets` SET `notified_at` = NOW() WHERE `id`=".$event->ticket_id);
            
            //update ticket to verify the event trigger timestamp
            $ticket = Ticket::where('id', $event->ticket_id)->with(['ticketUser'])->first();
            $ticket->notified_at = Carbon::now()->toDateTimeString();
            $ticket->save();
            
            //dd($ticket);
        
            \Mail::to($ticket->ticketUser->email)->send(new \App\Mail\ClosedTicketMail($ticket));

        } catch(\Exception $e) {

            //dd($e->getMessage());
            
        }

        //die('event listener inn...');
    }
}
