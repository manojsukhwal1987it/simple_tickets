@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> View Ticket</h2>
            </div>
            <div class="pull-right" style="text-align: right;">
                <a class="btn btn-primary" href="{{ route('tickets.index') }}"> Ticket List</a>
            </div>
        </div>
    </div>
   <br /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $ticket->title }}
            </div>
        </div>
        <br /><br />
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $ticket->description }}
            </div>
        </div>
        <br /><br />
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                {{ $ticket->status }}
            </div>
        </div>
        <br /><br />
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Created At:</strong>
                {{ $ticket->created_at }}
            </div>
        </div>
        <br /><br />
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Modified At:</strong>
                {{ $ticket->updated_at }}
            </div>
        </div>
        @if(!empty($ticket->notified_at))
        <br /><br />
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Notified At:</strong>
                {{ $ticket->notified_at }}
            </div>
        </div>
        @endif

    </div>
@endsection