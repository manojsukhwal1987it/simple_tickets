@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Ticket</h2>
            </div>
            <div class="pull-right" style="text-align: right;">
                <?php  ?><a class="btn btn-warning" href="{{ route('tickets.index') }}"> Ticket List </a><?php ?>
            </div>
        </div>
    </div>
       
    @if ($errors->any())
        <div class="alert alert-danger">
            There were some problems with your input.<br><br>
        </div>
<!--         <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul> -->
    @endif
       
    <form action="{{ route('tickets.store') }}" method="POST">
        @csrf
      
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Title:</strong>
                    <input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title')}}">
                    @error('title')
                        <div class="error alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <br /><br />
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Description">{{ old('description')}}</textarea>
                    @error('description')
                        <div class="error alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <br /><br />
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
       
    </form>
@endsection