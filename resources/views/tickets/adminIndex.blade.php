@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Admin Tickets List</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
   
   <div>
    <form method="get" id="filter" action="{{ route('admin.tickets.index')}}">
        <strong>Title : </strong>  &nbsp; &nbsp; <input type="text" name="title" value="{{ $_GET['title']??''}}"> &nbsp; &nbsp; <input type="submit" name="submit" value="Filter"> &nbsp; &nbsp; <input type="reset" onclick="location.href='{{route('admin.tickets.index')}}'" name="reset" value="Reset">
    </form>

   </div>

    <table class="table table-bordered">
        <tr>
            <th>SNo.</th>
            <th>Title</th>
            <th>User</th>
            <th>Status</th>
            <th>Last Modified</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tickets as $ticket)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $ticket->title }}</td>
            <td>{{ ($ticket->ticketUser->name) ?? "-"  }}</td>
            <td>{{ $ticket->status }}</td>
            <td>{{ $ticket->updated_at }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('admin.tickets.show',$ticket->id) }}">Show</a>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $tickets->links() !!}
      
@endsection