@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> My Tickets List</h2>
            </div>
            <div class="pull-right" style="text-align: right;">
                <?php  ?><a class="btn btn-success" href="{{ route('tickets.create') }}"> Create New Ticket</a><?php ?>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>SNo.</th>
            <th><a href="?sort=title&dir={{ (!empty($_GET['dir']) && $_GET['dir']=='asc')?'desc':'asc'}}">Title</a></th>
            <th><a href="?sort=status&dir={{ (!empty($_GET['dir']) && $_GET['dir']=='asc')?'desc':'asc'}}">Status</a></th>
            <th>Last Modified</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tickets as $ticket)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $ticket->title }}</td>
            <td>{{ $ticket->status }}</td>
            <td>{{ $ticket->updated_at }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('tickets.show',$ticket->id) }}">Show</a>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $tickets->links() !!}
      
@endsection