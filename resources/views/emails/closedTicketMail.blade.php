<!DOCTYPE html>
<html>
<head>
    <title>\Config::get('app.name')</title>
</head>
<body>
	<p>Hi {{ $ticket->ticketUser->name??"" }},</p>
	<br />
	<p>Your ticket status has been updated.
	Please find the details below: </p>
    <p>Title : {{ $ticket->title??"" }}</p>
    <p>Status : {{ $ticket->status??"" }}</p>    
    <p>Updated At : {{ $ticket->updated_at??"" }}</p>
    <p>Description : {{ $ticket->description??"" }}</p>
   
    <p>Thank you</p>
    <br />

    <p>From : </p>
    <p>Team {{ \Config::get('app.name') }}</p>
</body>
</html>
<?php //die('check email text'); ?>