<p align="center"> Simple Ticket System </p>

## About Application : Simple Ticket System

This is the simple ticket system, in which the new front user can login and create the ticket with title description and view the list/details of his own tickets. The admin user(with special access) can login and list/filter the user created tickets and can mark the ticket as closed in view/show page of the ticket. A notification has been sent to the assigned user on status change of the ticket.

## User Types
There are two types of users in the application :
1. Admin User : which can be list out all the tickets of front user and can marked the ticket as closed.
2. Front User : can register/login and create the new ticket and list/view their own tickets.


## User Credentials

Admin User : 
adminuser@yopmail.com/12345678

Front User : 
manoj@yopmail.com/12345678
manoj1@yopmail.com/12345678
manoj2@yopmail.com/12345678


## How to use
1. Copy & unzip or git clone the project in public directory, navigate to root folder and run "composer install" command in terminal.
2. Change the Database details/SMTP Email details in .env file in root folder.
3. Run the "php artisan migrate" command.
4. Then run the "php artisan serve" command.
5. Open the url in the browser and login as front user with above specified details or register as new front user and create the ticket by navigating the header menu "Ticket" link.
6. Open the url in seperate browser/tab and login as admin user with above specified details and navigate to ticket manager via menu "Ticket" links and list out the front user tickets.
7. In the View/Show detail ticket page, By clicking the button named as "Mark The Ticket Close", a notification will be sent to associated user about the ticket status changes after marking the ticket closed.


